<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class VerfiyOTPController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showVerifyPage()
    {
        return view('verifyPage');
    }

    public function verify(Request $request)
    {
        if(request('OTP') == auth()->user()->useOTPCode()){
            auth()->user()->update(['isvalid' => true]);
            return redirect('/home');
        }
    }
}

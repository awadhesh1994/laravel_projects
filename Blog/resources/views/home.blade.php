{{-- @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <table class='table table-striped'>
                        @if (count($post) > 0)
                            <tr>
                                <th> Title </th>
                                <th></th>
                                <th></th>
                            </tr>
                            @foreach ($post as $item)
                                <tr>
                                    <th> {{ $item->title  }} </th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            @endforeach
                        @else
                            <h1> Sorry Not data </h1>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
    @if (count($post) > 0)
        <div class="card-columns">
            @foreach ($post as $item)
                <div class="card card-background" style="background-image: url('assets/img/kit/pro/examples/card-blog3.jpg')">
                    <div class="card-body">
                        <h6 class="card-category text-info">{{ $item->user->name }}</h6>
                        <h3 class="card-title">The Sculpture Where Details Matter</h3>
                        <p class="card-description">
                            Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...
                        </p>
                        <a href="#pablo" class="btn btn-danger btn-round mt-4">
                            <i class="material-icons">subject</i> Read Article
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
    @endif
</div>
@endsection --}}


@extends('layouts.app')

@section('content')
<div class="container">
    @if (count($post) > 0)
        <div class="card-columns">
            @foreach ($post as $item)
                <div    @if ($item->img_cover == 'noimage.jpg')
                            class="card p-3 bg-success text-white"
                        @else
                            class="card p-3 card-background" style="background-image: url('/storage/img/{{ $item->img_cover }}')"
                        @endif
                    >
                    <div class="card-body">
                        <h3 class="card-title card-title-new font-weight-bold text-capitalize">{{ $item->title }}</h3>
                        <p class="card-description">
                            {{ $item->body }}
                        </p>
                        <a href="postURLs/{{ $item->id }}" class="btn btn-danger btn-round mt-4">
                            Read Article
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
    @else
        <div class="jumbotron jumbotron-fluid text-center">
            <div class="container">
              <h1 class="display-4 font-weight-bold">WOO...OO</h1>
              <p class="lead">No Post Found</p>
            </div>
        </div>
    @endif
</div>
@endsection

@component('mail::message')
# Hello {{ $username }}

This is your one time password <p style="color: #673AB7;font-weight: 600;font-size: 18px;">{{ $OTP }}</p>

Thanks,<br>
{{ config('app.name') }}
@endcomponent

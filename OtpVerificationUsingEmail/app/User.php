<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Mail;
use App\Mail\OTPMail;
use Illuminate\Support\Facades\Cache;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'isvalid'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // Use made functions

    public function uniqueKey(){
        return "OPT_Unique_{$this->id}";
    }

    public function createOTPCode(){
        $OTP = rand(100000, 999999);
        Cache::put([$this->uniqueKey() => $OTP], now()->addSecond(30));
        return $OTP;
    }

    public function sendOTPUsingMail(){
        $OTP = rand(10, 99);
        Mail::to($this->email)->send(new OTPMail($this->createOTPCode()));
    }

    public function useOTPCode(){
        return Cache::get($this->uniqueKey());
    }

}

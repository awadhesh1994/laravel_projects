@extends('layouts.app')

@section('content')
    <h1 class="well bg-warning pt-3 pb-3 mb-3 font-weight-bold text-center"> Create Post </h1>
    <div class="well bg-secondary p-3">
        {!! Form::open(['action' => 'PostsController@store', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
            <div class="form-group">
                {{ Form::label('title', 'Title', ['class' => 'text-white font-weight-bold']) }}
                {{ Form::text('title', '', ['class' => 'form-control', 'placeholder' => 'Enter title name']) }}
            </div>
            <div class="form-group">
                {{ Form::label('body', 'Body', ['class' => 'text-white font-weight-bold']) }}
                {{ Form::textarea('body', '', ['class' => 'form-control', 'placeholder' => 'Enter Text']) }}
            </div>
            <div class="form-group">
                {{ Form::file('img_cover') }}
            </div>
            <div class="from-group">
                {{ Form::submit('Save', ['class' => 'btn btn-primary']) }}
            </div>
        {!! Form::close() !!}
    </div>
@endsection

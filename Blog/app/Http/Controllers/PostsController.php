<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Post;

use DB;

class PostsController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$info = Post::all();
        //$info = DB::select(select * from posts);

        //$info = Post::orderBy('title', 'asc')->take(2)->get();
        $info = Post::orderBy('created_at', 'asc')->get();

        // $info = Post::orderBy('created_at', 'asc')->paginate(10);
        return view('posts.index')->with('data', $info);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
            'img_cover' => 'image|nullable|max:1999'
        ]);

        if($request->hasFile('img_cover')){
            $fileNameFind = $request->file('img_cover')->getClientOriginalName();
            $filename = pathinfo($fileNameFind, PATHINFO_FILENAME);
            $fileExtensionFind = $request->file('img_cover')->getClientOriginalExtension();
            $fileNameToStore = $filename.'.'.time().'.'.$fileExtensionFind;
            $path = $request->file('img_cover')->storeAs('/public/img', $fileNameToStore);
        }else{
            $fileNameToStore = 'noimage.jpg';
        }

        $postSave = new Post;
        $postSave->title = $request->input('title');
        $postSave->body = $request->input('body');
        $postSave->user_id = auth()->user()->id;
        $postSave->img_cover = $fileNameToStore;
        $postSave->save();

        return redirect('/postURLs')->with('success', 'Data has been stored !!!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $showInfo =  Post::find($id);
        return view('posts.show')->with('show', $showInfo);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editInfo = Post::find($id);
        if(auth()->user()->id !== $editInfo->user_id){
            return redirect('/postURLs')->with('error', 'sorry :-)');
        }
        return view('posts.edit')->with('editInfo', $editInfo);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required'
        ]);

        if($request->hasfile('img_cover')){
            $updateFileName = $request->file('img_cover')->getClientOriginalName();
            $updateName = pathinfo($updateFileName, PATHINFO_FILENAME);
            $updateFileExtension = $request->file('img_cover')->getClientOriginalExtension();
            $updateFileStore = $updateName.'.'.time().'.'.$updateFileExtension;
            $path = $request->file('img_cover')->storeAs('/public/img', $updateFileStore);
        }

        $updatePost = Post::find($id);
        $updatePost->title = $request->input('title');
        $updatePost->body = $request->input('body');
        if($request->hasFile('img_cover')){
            $updatePost->img_cover = $updateFileStore;
        }
        $updatePost->save();

        return redirect('/postURLs')->with('success', 'data updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $postRemove = Post::find($id);
        if(auth()->user()->id !== $postRemove->user_id){
            return redirect('/postURLs')->with('error', 'sorry :-)');
        }
        if($postRemove->img_cover != 'noimage.jpg'){
            Storage::delete('/public/img/'.$postRemove->img_cover);
        }
        $postRemove->delete();
        return redirect('/postURLs')->with('success', 'deleted post');
    }
}

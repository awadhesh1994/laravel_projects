@extends('app')

@section('content')
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                    Welcome For Testing Mail Service
            </div>
            <div class="modal-footer">
                <a class="text-decoration-none" href="/send">
                    <button type="button" class="btn btn-primary">
                        Send
                    </button>
                </a>
            </div>
        </div>
    </div>
@endsection



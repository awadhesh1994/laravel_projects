@extends('layouts.app')

@section('content')
    <button class="btn btn-danger text mb-2"><a class="text-white text-decoration-none" href="/postURLs">Go Back</a></button>
    <div class="well bg-info p-2 ">
        <h2 class="text-uppercase text-white text-center"> {{ $show->title }} </h2>
    </div>
    <div class="well pt-1 pl-2 pr-2 pb-1">
        <span class="text-uppercase badge badge-warning p-2"> {{ $show->created_at }} </span>
        @if (!Auth::guest())
            @if (Auth::user()->id == $show->user_id)
                <span class="ml-2 text-uppercase badge badge-dark p-2">
                    <a class="text-white text-decoration-none" href="/postURLs/{{ $show->id }}/edit">
                        Edit
                    </a>
                </span>
            @endif
        @endif
    </div>
    <div class="well mt-2 text-capitalize p-2 bg-success">
        {!! $show->body !!}
    </div>
    @if (!Auth::guest())
        @if (Auth::user()->id == $show->user_id)
            <span class="ml-2 text-uppercase">
                {!! Form::open(['action' => ['PostsController@destroy', $show->id], 'method' => 'POST']) !!}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                {!! Form::close() !!}
            </span>
        @endif
    @endif
@endsection

<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

#  :elephant: Laravel  :elephant:

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, yet powerful, providing tools needed for large, robust applications.

### User Verification using EmailOTP

The Email OTP authentication method sends an email to your email address with a one-time password (OTP). You can use this OTP to authenticate within a certain time frame.
This authenticator enrolls automatically and it's not possible to remove it.

### Environment Configuration (.env files)

### Mail Configuration

```markdown
- MAIL_DRIVER = smtp
- MAIL_HOST = smtp.domain.com
- MAIL_PORT = 465
- MAIL_USERNAME = email
- MAIL_PASSWORD = email_password
- MAIL_ENCRYPTION = ssl
```

### Database Configuration

```markdown
- DB_CONNECTION = mysql
- DB_HOST = 127.0.0.1
- DB_PORT = 3306
- DB_DATABASE = database
- DB_USERNAME = username
- DB_PASSWORD = password
```

### Modifying Create user migration

- add new column <code> $table->boolean('isvalid')->default(false); </code> in up function() only

### Database Migrate

- <code>php artisan migrate</code>&nbsp;&nbsp;<b>&nbsp;&nbsp;OR&nbsp;&nbsp;</b>&nbsp;&nbsp;<code>php artisan migrate:fresh</code>

### Authentication

Laravel ships with several pre-built authentication controllers, which are located in the  App\Http\Controllers\Auth namespace. The RegisterController handles new user registration, the LoginController handles authentication, the ForgotPasswordController handles e-mailing links for resetting passwords, and the ResetPasswordController contains the logic to reset passwords. Each of these controllers uses a trait to include their necessary methods. For many applications, you will not need to modify these controllers at all.

### Authentication Routing 

Laravel provides a quick way to scaffold all of the routes and views you need for authentication using one simple command:

- <code>php artisan make:auth</code>

### Create new middleware 

- <code> php artisan make:middleware TwoWayAuth </code>
- Go to <b>Kernel.php</b> and add this middleware in <b>$routeMiddleware</b> array 
- Like this:- <code>'TwoWayAuth' => \App\Http\Middleware\TwoWayAuth::class,</code>
- Now add this middleware with route <b>web.php</b>

```markdown
Route::group(['middleware' => 'TwoWayAuth'], function(){
    Route::get('/home', 'HomeController@index')->name('home');
});
```

- Modify TwoWayAuth.php also

```markdown
public function handle($request, Closure $next)
{
    if(auth()->user()->isvalid){
        return $next($request);
    }
    return redirect('/OTPVerification');
}
```

### Modify LoginController.php

- Overridding function called attemptLogin 
- You can find this function under <code>trait AuthenticatesUsers</code> <b>AuthenticatesUsers.php</b>
- Copy attemptLogin function trait AuthenticatesUsers and past same function in LoginController.php
- OR copy this 

```markdown
protected function attemptLogin(Request $request)
    {
        $vaildLogin = $this->guard()->attempt(
            $this->credentials($request), $request->filled('remember')
        );
        if($vaildLogin){
            auth()->user()->sendOTPUsingMail();
         }
        return redirect('/');
    }
```

### Create new mailer

```markdown
php artisan make:mail OTPMail --markdown=OTPCODE
```

### Modify User.php

- Add four new function
- First, <b>public function uniqueKey()</b> it used to identify the use id.

```markdown
public function uniqueKey(){
    return "OPT_Unique_{$this->id}";
}
```
    
- Second, <b>createOTPCode()</b> it used to create OTP and also verifies that code is generated or not generated.  

```markdown
public function createOTPCode(){
        $OTP = rand(100000, 999999);
        Cache::put([$this->uniqueKey() => $OTP], now()->addSecond(30));
        return $OTP;
    }
```    

- Third, <b>sendOTPUsingMail()</b> it used to send OTP.

```markdown
public function sendOTPUsingMail(){
    Mail::to($this->email)->send(new OTPMail($this->createOTPCode()));
}
```

- Last, <b>useOTPCode()</b> it used to take OPT.

```markdown
public function useOTPCode(){
    return Cache::get($this->uniqueKey());
}
```
    
### Modify OTPMail.php

```markdown
<code>class OTPMail extends Mailable
{
   use Queueable, SerializesModels;
   $OTP;
   public function __construct($OTP)
   {
        $this->OTP = $OTP;
   }
   public function build()
   {
        return $this->markdown('OTPCODE')->with(['OTP' => $this->OTP]);
   }
}
```

### Define two new route

```markdown
Route::get('/OTPVerification', 'VerfiyOTPController@showVerifyPage');
Route::post('/OTPVerification', 'VerfiyOTPController@verify');
```

### Create new Controller VerfiyOTPController.php

- Define two function <b>showVerifyPage()</b> and <b>verify()</b>

 ```markdown
 public function showVerifyPage()
{
    return view('verifyPage');
}
```
    
```markdown
public function verify(Request $request)
    {
    if(request('OTP') == auth()->user()->useOTPCode()){
        auth()->user()->update(['isvalid' => true]);
            return redirect('/home');
        }
    }
```
    

### Create new View verifyPage.blade.php

- Check file /resources/views/verifyPage.blade.php
- <code>markdown add method="POST" and action="/OTPVerification"</code>  in html form tag

### Optional modification LoginController.php

- Overridding function called logout
- You can find this function under <code>trait AuthenticatesUsers</code> <b>AuthenticatesUsers.php</b>
- Copy logout function trait AuthenticatesUsers and past same function in LoginController.php
- OR copy this

```markdown
public function logout(Request $request)
    {
        //auth()->user()->update(['isvalid' => false]); <- this line is optional if you wanted to ask again and again for otp
        $this->guard()->logout();
        $request->session()->invalidate();
        return $this->loggedOut($request) ?: redirect('/');
    }
```

<br/>
:jack_o_lantern: :clap: :clap: :jack_o_lantern:
@extends('app')

@section('content')
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                    Mail Send Successfully
            </div>
            <div class="modal-footer">
                <a class="text-decoration-none" href="/">
                    <button type="button" class="btn btn-primary">
                        Go Home
                    </button>
                </a>
            </div>
        </div>
    </div>
@endsection

@extends('layouts.app')

@section('content')
    <h1 class="well bg-warning pt-3 pb-3 mb-3 font-weight-bold text-center"> Update Post </h1>
    <div class="well bg-secondary p-3">
        {!! Form::open(['action' => ['PostsController@update', $editInfo->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
            <div class="form-group">
                {{ Form::label('title', 'Title', ['class' => 'text-white font-weight-bold']) }}
                {{ Form::text('title', $editInfo->title, ['class' => 'form-control', 'placeholder' => 'Enter title name']) }}
            </div>
            <div class="form-group">
                {{ Form::label('body', 'Body', ['class' => 'text-white font-weight-bold']) }}
                {{ Form::textarea('body', $editInfo->body, ['class' => 'form-control', 'placeholder' => 'Enter Text']) }}
            </div>
            <div class="form-group">
                {{ Form::file('img_cover') }}
            </div>
            <div class="from-group">
                {{ Form::hidden('_method', 'PUT') }}
                {{ Form::submit('Update', ['class' => 'btn btn-primary']) }}
            </div>
        {!! Form::close() !!}
    </div>
@endsection

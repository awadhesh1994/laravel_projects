<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

### About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, yet powerful, providing tools needed for large, robust applications.


### Email Verification

Many web applications require users to verify their email addresses before using the application. Rather than forcing you to re-implement this on each application, Laravel provides convenient methods for sending and verifying email verification requests.

### Environment Configuration (.env files)

### Mail Configuration

<code>
- MAIL_DRIVER = smtp
- MAIL_HOST = smtp.domain.com
- MAIL_PORT = 465
- MAIL_USERNAME = email
- MAIL_PASSWORD = email_password
- MAIL_ENCRYPTION = ssl
</code>

### Database Configuration

<code>
- DB_CONNECTION = mysql
- DB_HOST = 127.0.0.1
- DB_PORT = 3306
- DB_DATABASE = database
- DB_USERNAME = username
- DB_PASSWORD = password
</code>

### Database Migrate

- <code>php artisan migrate</code>&nbsp;&nbsp;<b>&nbsp;&nbsp;OR&nbsp;&nbsp;</b>&nbsp;&nbsp;<code>php artisan migrate:fresh</code>

### Authentication

Laravel ships with several pre-built authentication controllers, which are located in the  App\Http\Controllers\Auth namespace. The RegisterController handles new user registration, the LoginController handles authentication, the ForgotPasswordController handles e-mailing links for resetting passwords, and the ResetPasswordController contains the logic to reset passwords. Each of these controllers uses a trait to include their necessary methods. For many applications, you will not need to modify these controllers at all.

### Authentication Routing 

Laravel provides a quick way to scaffold all of the routes and views you need for authentication using one simple command:

- <code>php artisan make:auth</code>

### Model Preparation

To get started, verify that your App\User model implements the  Illuminate\Contracts\Auth\MustVerifyEmail contract:

- Go to /app/User.php model and add interface => MustVerifyEmail
- Like :- <code>class User extends Authenticatable implements MustVerifyEmail</code>

### Routing

- Initially it look like -->>  Auth::routes();
- Change to -->> <code> Auth::routes(['verify' => true]); </code>

### Protecting Routes

#### One way to define middleware

<code>&nbsp;Route::get('/testing', function() {
&nbsp;&nbsp;&nbsp;&nbsp;return view('Testing');
&nbsp;&nbsp;})->middleware('verified');</code>

#### Another way to define middleware

<code>&nbsp;Route::group(['middleware' => 'verified'], function() {
&nbsp;&nbsp;&nbsp;&nbsp;return view('Testing');
&nbsp;&nbsp;});</code>

- Create new view name <code> Testing.blade.php<code>

### Generating layout

- Run <code> php artisan vendor:publish --tag=laravel-mail </code>

- This will <code>download the markdown (html,css and layout files) into views/vendor/mail directory</code>. You can then modify the structure for the email template and also change the css from views/vendor/mail/themes/default.css

### Passing username to mail

#### For email verification

1. Go to file <b>MustVerifyEmail.php</b> and modify function

    <code>public function sendEmailVerificationNotification()
    {
    &nbsp;&nbsp;&nbsp;$this->notify(new Notifications\VerifyEmail($this->name));
    }</code>


2. Then go to file <b>VerifyEmail.php and add </b>

    <code>public $name;
    public function __construct($name)
    {
    &nbsp;&nbsp;&nbsp;$this->name = $name;
    }</code>
  
   also <b>modify toMail function and add </b> 
   
   <code>->greeting('Hello ' . $this->name . ',')</code>&nbsp;&nbsp;to return

#### For password reset

1. Go to file <b>CanResetPassword.php</b> and modify function

    <code>public function sendPasswordResetNotification($token)
    {
    &nbsp;&nbsp;&nbsp;$this->notify(new ResetPasswordNotification($token, $this->name));
    }</code>
    
2. Then go to file <b>ResetPassword.php and add </b>

    <b> Add </b><br/>
    <code>public $name;</code>
    
    <b>Modify Function</b><br/>
    <code> public function __construct($token, $name)
    {
    &nbsp;&nbsp;&nbsp;$this->token = $token;
    &nbsp;&nbsp;&nbsp;$this->name = $name;
    }</code>
    
    also <b>modify toMail function and add </b> 
   
   <code>->greeting('Hello ' . $this->name . ',')</code>&nbsp;&nbsp;to return
   
   <code> php artisan serve </code> to start server
   
 <br/>
 :tada: :tada: :tada: :smiley: :smiley: :clap: :clap: :clap: :tada: :tada:  :tada:
 

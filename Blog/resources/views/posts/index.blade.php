@extends('layouts.app')

@section('content')
            <div class="card-columns">
        @if (count($data) > 0)
                @foreach ($data as $item)
                   <div class="card p-3 m-2">
                        @if ($item->img_cover == 'noimage.jpg')

                        @else
                            <img style="width:100%" src="/storage/img/{{ $item->img_cover }}"/>
                        @endif
                        <p class="font-weight-bold text-uppercase pt-2"><a class="text-secondary text-decoration-none" href="postURLs/{{ $item->id }}"> <i class="fa fa-newspaper-o"></i>{{ $item->title }}</a></p>
                        <small class="text-capitalize text-danger text-left font-weight-bold"> {{ $item->updated_at }}</small>
                        <p class="mt-2 mb-2">{{ $item->body }}</p>
                        <small class="text-uppercase text-danger text-left font-weight-bold"> post by {{ $item->user->name }} </small>
                    </div>
                @endforeach
        @else
            <h1> Ho </h1>
        @endif

    </div>
@endsection

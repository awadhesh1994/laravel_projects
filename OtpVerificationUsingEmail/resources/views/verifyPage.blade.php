@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Enter OTP</div>
                <div class="card-body">
                    <form class="form-inline" method="POST" action="/OTPVerification">
                        @csrf
                        <div class="form-group mb-2">
                            <label>OTP</label>
                        </div>
                        <div class="form-group mx-sm-3 mb-2">
                            <input type="number" id="opt" name="OTP" class="form-control" placeholder="Enter OTP" required>
                        </div>
                        <button type="submit" class="btn btn-primary mb-2">Verifyed</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection



<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;

class MailServiceController extends Controller
{
    public function send(){
        $data = "Mail has been send successfully";
        Mail::send(['text' => 'message'], ['name' => 'send name'], function($info){
            $info->to('receiver email', 'receiver name')->subject('Testing Email Service');
            $info->from('send email', 'send name');
        });
        return view('success');
    }
}
